---
title: What does the kubelet say?
date: 2019-01-31
---

## Abstract
Etcd says store, kube-proxy says route, API server says 418, what does the kubelet say?

Kubelet is one of the central components in the Kubernetes cluster. Most of us are taking for granted that it would just work and start our containers. CNI handles the networking part, kube-proxy the service part, but kubelet actually does much more than just starting containers. In this talk, I cover kubelet on a high level before deep diving in the belly of the beast and its interfacing with CNI, container runtime and ultimately Linux kernel.

## Primary Speaker Biography (provide a biography that includes your employer (if any), ongoing projects and your previous speaking experience).*

Neven’s parents committed a grave mistake, and at the age of 12 introduced him to Pascal and Logo. Since then he's programmed in various languages, from C, C++ to Haskell and Go. He saw his first big system during his Facebook 2015 summer internship, and since then worked on various jobs from building a DNA deep learning base-caller to a real-time cryptocurrency bot in Go.  

Currently, his passion lies in DevOps, Kubernetes and the CNCF landscape, as well as performance engineering, monitoring, and system observability.

## Category

Cloud Foundry 
