---
title: WireGuard & kubernetes for the masses
date: 2019-04-30
---

## Abstract
Managing VPN configurations at scale is difficult, error-prone, and gives a headache to the operators. 

Using VPN with the Kubernetes CNI solution brings additional complexity to the network layer. It makes it harder to set up, debug and observe. 

This talk sums up my experience with WireGuard and it's integration with the Kubernetes ecosystem: 

First, I’ll introduce the WireGuard CNI ansible role: It allows using the same interface for both VPN and pod-to-pod communication while keeping internode communication encrypted. Furthermore, by using the native Linux routing, it's easily debuggable and tunable with common networking tools. 

Secondly, I briefly present `wg-quick-go`, a Go library as a superset of the `wg-quick` tool. 

For the grand finale, I show the `wg-operator`, a full solution for managing Wireguard VPN. For now, only the Kubernetes backend is implemented, though there's no reason not to plug in other K/V providers such as etcd/consul/zookeeper.


## Primary Speaker Biography (provide a biography that includes your employer (if any), ongoing projects and your previous speaking experience).*

Neven’s parents committed a grave mistake, and at the age of 12 introduced him to Pascal and Logo. Since then he's programmed in various languages, from C, C++ to Haskell and Go. He saw his first big system during his Facebook 2015 summer internship, and since then worked on various jobs from building a DNA deep learning base-caller to a real-time cryptocurrency bot in Go.  

Currently, his passion lies in DevOps, Kubernetes and the CNCF landscape, as well as performance engineering, monitoring, and system observability.

Neven also created wireguard CNI ansible roles, wg-quick-go wireguard library and wg-operator designed to manage VPN private/public keys via the kubernetes API server.

## Category

Cloud Foundry for Operators

## Audience

Operators
