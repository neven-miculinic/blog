---
title:  go:generate
---

#  Go:generate

## Elevator pitch (300 characters)

G*** might be a dirty word in go, but there are various pre-compile time code generation possibilities. Join if you're interested in various possibilities, overview, tips & tricks.

## Description 

go:generate is useful tool aiding developers reducing boilerplate code. Whether you're having a simple enum type and want to generate `fmt.Stringer` implementation for it, all the way to kubernetes object boilerplate generation or parsing AST(Abstract syntax tree).

This talk goes over possibilities and common patterns in go pre-compile code generation. Topics include, but not limited to gRPC/protobuf, AST parsing and manipulation, `text/template`, mock generation, tools overview, and more. This talk is an overview of the current ecosystem state, with many examples. 

There should be something for everyone.

## Notes

I've been using Go for a few years and go:generate has been useful companion in that journey. When exploring this topic I've learned a lot of tricks which I've presented in my local meetup group (with same named talk). It was well received, and together we learned how to write less code via keyboard and more via `go:generate`. 

## Bio

His parents committed a grave mistake, and at age 12 introduced him to Pascal and Logo. Since then he's programmed in various languages, from C, C++ to Haskell and Go. He saw his first big system during Facebook 2015 summer internship, and since then worked on various jobs from building DNA deep learning base-caller to real-time cryptocurrency bot in go.  

Currently, I'm interested in DevOps, Kubetnetres and CNCF landscape, as well as performance engineering, monitoring and system observability