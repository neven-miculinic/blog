---
title:  Dr. Go memory internals or: How I Learned to Stop Worrying and Love the GC
---

#  Dr. Go memory internals or: How I Learned to Stop Worrying and Love the GC

## Elevator pitch (300 characters)

Memory layout and strategy can make or break the application. It has a profound effect on memory consumption, latency, and performance. Learn the internals for the greater good.

## Description 

In designing go memory there's a lot of intricate little detail, and less intricate bigger picture concepts useful for everyday go programmers. 

When you're developing your application, do you think about escape analysis and pointer vs. non-pointer trade-off on the critical path? In case you don't understand those trade-off, or terms this talk is for you. 

Also, this talk covers how CGO fits into go memory internals, briefly touches on Go vs C ABI calling conventions and other low-level details having high-level consequences.

## Notes

I've spent quite some time studying this problem, read the blog posts, reviewed relevant talks, browsed the source code, and frequently lurk on the #performance channel on gopher slack.

This talk is a new one in my repertoire, thus it has the opportunity for its big premiere on the GopherCon EU. 

The benefit for the audience is better understanding the go memory management internals. For most cases, you won't need them too much beyond the basics, but it's useful to be aware once you're debugging weird behavior, or long tail latency issue, or optimizing the critical path.

## Bio

His parents committed a grave mistake, and at age 12 introduced him to Pascal and Logo. Since then he's programmed in various languages, from C, C++ to Haskell and Go. He saw his first big system during Facebook 2015 summer internship, and since then worked on various jobs from building DNA deep learning base-caller to real-time cryptocurrency bot in go.  

Currently, I'm interested in DevOps, Kubetnetres and CNCF landscape, as well as performance engineering, monitoring and system observability