---
title:  Golang race detection
---

#  Golang data race

## Elevator pitch (300 characters)
Data races are nasty kinds of bugs; rare, hard to replicate and tend to occur at the worst possible moment. Go race detector solves this problems. Come and learn how it dose so.

## Description 

Data races are nasty kinds of bugs; rare, hard to replicate and tend to occur at the worst possible moment. Their effect is undefined, detection hard, almost impossible without expensive formal verification and static analysis tools...Or is it? 

This talk focuses on [ThreadSanitizer](https://clang.llvm.org/docs/ThreadSanitizer.html), a library for detecting race conditions at run-time. It originated in clang & C++ community and its use spread to go (-race), rust, java, and some other languages. 

It covers how it works conceptually, and necessary background for its understanding.

## Notes

** This field supports Markdown. Notes will only be seen by reviewers during the CFP process. This is where you should explain things such as technical requirements, why you're the best person to speak on this subject, etc... **

I've spent quite some time studying this problem, read academic paper history/related work on this subject, reviewed relevant talks, browsed the source code, also I'm organizing local distributed system meetup which focuses on these kinds of problems. 

Furthermore from the feedback I've received when I held this presentation before it was well received and lover. Everyone learned something new. 

The benefit for the audience is clear from the description, it helps you understand what you're using every day (or should be using), and what tradeoffs are involved in the design.

## Bio

His parents committed a grave mistake, and at age 12 introduced him to Pascal and Logo. Since then he's programmed in various languages, from C, C++ to Haskell and Go. He saw his first big system during Facebook 2015 summer internship, and since then worked on various jobs from building DNA deep learning base-caller to real-time cryptocurrency bot in go.  

Currently, I'm interested in DevOps, Kubetnetres and CNCF landscape, as well as performance engineering, monitoring and system observability