---
title: "Containers on IoT ARM devices: Kubernetes, docker and other hells"
date: 2019-01-31
---

Deploying kuberentes worker nodes on edge IoT devices brings considerable challenges. Those are often low powered ARM devices, sometimes not even having hardware floating point co-processor. They are RAM limited, and even their persistent store, eMMC, isn't top performing hardware. In this talk, I present optimization tuning and various tradeoffs for running kubernetes workers and container runtime on IoT ARM devices. 