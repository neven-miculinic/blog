---
title: "The linux programming interface"
---

# Chapter 14 - Filesystems

## syscalls

* mount
* unmout
* unmout2
* statvfs
* fstatvfs

## Notes

* ext2 -- a simple filesystem supporing gapped files. No journaling, slow fchk time. Data block, 1st-2nd-3rd indirection blocks allowing efficient seeks and huge file sizes while optimized for small files (the filesystem's ususl)
* ext3 -- ext2 with journaling support
* ext4, btrfs --> modern file systems
* mount --move ; for moving the mountpoints atomically. Cool feature I had no idea about
* It is not possible to unmount a file system that is busy; that is, if there are open files on the file system, or a process’s current working directory is somewhere in the file system. Calling umount() on a busy file system yields the error EBUSY.


# Chapter 15 -- File attributes

## syscalls
* stat
* lstat
* fstat
* utime/utimes
* utimensat
* futimens
* chown
* lchown
* fchown
* chmod
* fchmod
* access
* umask --> (mode = mode & ~umask); unset mask

## Notes

* folder can have set-Group-ID property and propagate it's group on newly created children

Folder permissions:
    * r: The contents (i.e., the list of filenames) of the directory may be listed.
    * w: Files may be created in and removed from the directory. Note that it is not necessary to have any permission on a file itself in order to be able to delete it.
    * x: Files within the directory may be accessed. Execute permission on a directory is sometimes called search permission.

To add or remove files in a directory, we need both execute and write permis- sions on the directory.
https://www.gnu.org/software/libc/manual/html_node/Permission-Bits.html

## Extended attributes

* Present in ext2, ext3, ext4, btrfs,... filesystems

# Chapter 16 - extended attributes

* set/get extended attributes; k/v pairs

```C
#include <sys/xattr.h>
int setxattr(const char *pathname, const char *name, const void *value, size_t size, int flags);
int lsetxattr(const char *pathname, const char *name, const void *value, size_t size, int flags);
int fsetxattr(int fd, const char *name, const void *value, size_t size, int flags);
```

```bash
setfattr -n name.x -v "value" xxx
getfattr -d xxx
```

# Chapter 17 -- access control lists

* more granular access to files/folders
* saved as extended attribute under system.posix_acl_access
* `getfacl` & `setfacl` command to manipulate

# Signals 20-22

Processor sleep states: 

* TASK_INTERRUPTIBLE 
* TASK_UNINTERRUPTIBLE
* TASK_KILLABLE