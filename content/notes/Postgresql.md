# Links:

* https://wiki.postgresql.org/wiki/Pgpool-II


## Natively

* supports synchrunous replication. https://www.enterprisedb.com/blog/cheat-sheet-configuring-streaming-synchronous-replication-postgresql
* With proper setup and `synchronous_commit = remote_apply` we literaly wait until sync replicas apply changes to the disk
* Reads distributed across node
* This is master-slave arhitecture, we need some automatic failover layer on top
*  Use `SELECT * FROM pg_stat_replication` to confirm we're in `sync_state` sync. I guess.
```
    repmgr=# SELECT * FROM pg_stat_replication;
    -[ RECORD 1 ]----+------------------------------
    pid              | 19111
    usesysid         | 16384
    usename          | repmgr
    application_name | node2
    client_addr      | 192.168.1.12
    client_hostname  |
    client_port      | 50378
    backend_start    | 2017-08-28 15:14:19.851581+09
    backend_xmin     |
    state            | streaming
    sent_location    | 0/7000318
    write_location   | 0/7000318
    flush_location   | 0/7000318
    replay_location  | 0/7000318
    sync_priority    | 0
    sync_state       | async
```

Tons of stuff available:

* https://github.com/dhamaniasad/awesome-postgres#high-availability

## Patroni
```A template for PostgreSQL High Availability with ZooKeeper, etcd, or Consul```

* https://github.com/zalando/patroni [2000 starts]
* has pgbouncer/haproxy templates support for updating
* provides systemd/upstart/system V init scipts
* https://patroni.readthedocs.io/en/latest/replication_modes.html#synchronous-mode  --> use strict replication mode
* ```Patroni will only ever assign one standby to synchronous_standby_names because with multiple candidates it is not possible to know which node was acting as synchronous during the failure.```
* it's sync mode is limiter to 3 nodes effectively; since it uses only one `synchronous_standby_name` and shall report write as commited after master + single replica finishes. In effect quorum is acchieved in only 3 node scenario with 2 nodes agreeing for sure.
* Streaming replication in 140 characters (from https://jobs.zalando.com/tech/blog/zalandos-patroni-a-template-for-high-availability-postgresql/, slide 28):
`(etcd&) && git clone https://github.com/zalando/patroni && cd patroni && pip install -r requirements.txt && for i in 0..1;do;(python patroni.py postgres$i.yml&); done`

## Stolon

```PostgreSQL cloud native High Availability and more.```

```
So another way to achive PostgreSQL high availability is needed. Following the shared nothing approach of other noSQLs we can leverage the great postgres’s streaming replication to achieve this. You can easily create a master and some replicas with asynchronous or synchronous replication. The next step is to automate the failover when the master instance is unhealthy. There are various good tools to achieve this (for example repmgr or governor).

After experimenting with them we noticed that they were difficult to implement in an IaaS or inside a container orchestrated environment (like kubernetes, nomad etc…) (changing IPs, different advertised IPs etc…). Additionally, since one of the main goal was to achieve the most possible data consistency, these tools didn’t provide the needed resilency to the possible types of network partitioning. For example it’s difficult for a client to point to the right master during a network partition, it can happen that a new master is elected also if the old one is active (but partitioned) and some of the clients’ sessions continue to communicate (and also do writes) to the old one causing data loss/inconsistencies. There’s the need to find a way for the client to connect to the right master and also forcibly close connections to the old one.
```

```
As I tried to explain in this introduction (that was done in early 2016), the patroni predecesssor was governor. I don't know how patroni changed from it but IMHO we think stolon is much more reliable at keeping data consistency.
```

* https://github.com/sorintlab/stolon [1700 stars]
* Sounds like the easiest/best option
* written in golang
* implement their own proxy instead of using ha-proxy/pg_bouncer
* hard quorum, compared to patroni they got it right and beutiful
* can deal with changing IPs, network partitioning and various other mess that happens
* As Patroni and repmgr is uses pg's failover under the hood, it's just keeping the one and only one master up and accepting conections

## PAF

First google link, well it must be good?

* https://www.postgresql.org/about/news/1780/ 
* https://pgstef.github.io/2018/02/07/introduction_to_postgresql_automatic_failover.html
* https://github.com/ClusterLabs/PAF  [162 starts, 9 contributors, commit ones appears deadish]

##  Slony-I

```
Slony-I is a "master to multiple slaves" replication system for PostgreSQL supporting cascading (e.g. - a node can feed another node which feeds another node...) and failover.
The big picture for the development of Slony-I is that it is a master-slave replication system that includes all features and capabilities needed to replicate large databases to a reasonably limited number of slave systems.
Slony-I is a system designed for use at data centers and backup sites, where the normal mode of operation is that all nodes are available.
A fairly extensive "admin guide" comprising material in the Git tree may be found here. There is also a local copy.
The original design document is available here; see also initial description of implementation..
```

* http://slony.info/
* website is properly shitty designed

## Bi-Directional Replication

* https://github.com/2ndQuadrant/bdr   [288 stars]
* async master-master


## Repmgr

```The Most Popular Replication Manager for PostgreSQL (Postgres)```

* https://github.com/2ndQuadrant/repmgr
* Same authors as 2ndQuadrant, commercial support available
* could find only old ansible roles, https://github.com/alexey-medvedchikov/ansible-repmgr being prime example
* https://github.com/2ndQuadrant/repmgr/issues/274 --> for how setting up syncronous replication
* Integrated with barman to create new instace from backup instead of live database in case of recovery
* https://github.com/2ndQuadrant/repmgr/blob/master/doc/repmgrd-node-fencing.md  -- there's some script to configure pgbouncer instances
* it feels...complicated and not working out of the box

## Citrus

* automatic scaling & sharding, HA included
* pg extension

### Skytools-legacy

* sounds legacy
* https://github.com/pgq/skytools-legacy


## k8s postgres operator

* https://crunchydata.github.io/postgres-operator 
* uses pgpool underneath the hood

# Backup

* https://github.com/2ndquadrant-it/barman/


# Monitorning

* https://github.com/ankane/pghero --> slow queries, duplicated index, etc. 

# Tuning

* https://github.com/HypoPG/hypopg --> Create hypothetical index, and see if it would be useful in speeding up queries