* [Rename network interfaces](https://www.shellhacks.com/change-network-interface-name-eth0-eth1-eth2/)

```
# PCI device 0x11ab:0x4363 (sky2)
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*",
ATTR{address}=="00:00:00:00:00:00",ATTR{dev_id}=="0x0", ATTR{type}=="1",
KERNEL=="eth*", NAME="eth0"
```

* https://wiki.archlinux.org/index.php/udev

* udevadm monitor --> watch udev rules

SUBSYSTEM=="net", ATTRS{idProduct}=="0195", ATTRS{idVendor}=="1bbb", NAME="usb-modem"
