# Firewall

* iptables
* ufw --> nice frontend for setting firewall iptables setting
* https://firewalld.org/ ....something like replacement for iptables? ; uses nftables under the hood

From wiki:
nftables is a subsystem of the Linux kernel providing filtering and classification of network packets/datagrams/frames. It has been available since Linux kernel 3.13 released on 19 January 2014.[2]

nftables is supposed to replace certain parts of netfilter, while keeping and reusing most of it. Among the advantages of nftables over netfilter is less code duplication and more throughput. nftables is configured via the user-space utility nft while netfilter is configured via the utilities iptables, ip6tables, arptables and ebtables frameworks.

nftables utilizes the building blocks of the Netfilter infrastructure, such as the existing hooks into the networking stack, connection tracking system, userspace queueing component, and logging subsystem.
