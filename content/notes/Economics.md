These are the notes for the Paul Samuelson, William Nordhaus - Economics-McGraw-Hill_Irwin (2009)

# Introduction


* advocates market forced with fair-minded government oversight; neither a purely unregulated market nor overregulated socialist market leads to prosperity
* Clear, minimal and up to the point. This textbook aims to prepare students for the 21st century

# Chapter 1: Central concepts of economics

Why study it? --> Because it influences our everyday decisions, abilities and the world around us. Those fundamental laws shape the world as we know it. It's instrumental in understanding various political issues as well as our employment prospects.

Economics is the study of how societies use scarce resources to produce valuable goods and services and distribute them among different individuals.

We live in a scarcity world; if there's infinite or adequate supply to meet all demands studying economics would be meaningless. 

Economic efficiency requires that an economy produce the highest combination of quantity and quality of goods and services given its technology and scarce resources. An economy is producing efficiently when no individual’s economic welfare can be improved unless someone else is made worse off.

Two subfields:
 * Microeconomics, founded by Adam Smith's 1776 The Wealth of Nations
 * Macroeconomics, founded by John Maynard Keynes's 1936 "General Theory of Employment, Interest, and Money"

Common economics fallacies:
 * post hoc --> temporal placement doesn't imply causation (raising wages and prices during the Great Depression in the 30s hasn't helped the recovery until pre-WW2 spending increase)
 * Failure to hold other things constant
 * fallacy of composition --> sum is greater than its parts --> raising tariffs on one product benefits it; raising all tariffs makes society worse off

Frankly, in economics due to complexity, there are few macro double-blind studies. Econometrics use statistics to help with this.


Positive vs normative economics: Positive focuses on facts (e.g. did average US raise due to NAFTA? Why doctors earn more than janitors?) vs. values of fairness (Has the distribution of income in the United States become too unequal? Should unemployment be raised to ensure that price inflation does not become too rapid?)


The central question of economics in any society -- what goods are produced, who produces them and for whom (i.e. distribution) are they produced.

Economic life is organized either through a hierarchical command or decentralized voluntary markets. Today most decisions in the United States and other high-income economies are made in the marketplace. But the government plays an important role in overseeing the functioning of the market; governments pass laws that regulate economic life, produce educational and police services, and control pollution. Most societies today operate mixed economies.

Each good or service has its inputs and outputs. Inputs roughly fall into 3 categories: land(i.e. environment), labor, and capital(roads, machines, software, ...)

The production possibility frontier (or PPF) shows the maximum quantity of goods that can be efficiently produced by an economy, given its technological knowledge and the quantity of available inputs. (( guns & butter example ))

The opportunity cost of a decision is the value of the good or service forgone.

Productive efficiency occurs when an economy cannot produce more of one good without producing less of another good; this implies that the economy is on its production possibility frontier.

# Chapter 2: modern mixed economy

## The market mechanics

* A market is a mechanism through which buyers and sellers interact to determine prices, and exchange goods, assets and services
* Prices server as a signal in the market economy. High prices reduce consumptions and encourage production. Lower prices do the opposite.
* Adam Smith discovered a remarkable property of a competitive market economy. Under perfect competition and with no market failures, markets will squeeze as many useful goods and services out of the available resources as is possible. But where monopolies or pollution or similar market failures become pervasive, the remarkable efficiency properties of the invisible hand may be destroyed.

* invisible hand --> inefficient markets the maximizing individual profits maximize societies profits as well.
* sum is greater than the sum of its parts --> via worker specialization more is produced than via generic X workers.

## Trade, money & capital 

* Via specialization society's whole society production increases; then we trade our surplus for our wishes.
* Money lubricates trade being universal mean of exchange
* Capital goods — produced inputs such as machinery, structures, and inventories of goods increase production efficiencies further down the line. I.e. fishing rod is a capital good for maximizing fishing productivity. Even better capital good is AI fishing boats with fully autonomous nets

# Visible hand of the governments

* Historically Laissez-fair market economies of the 19th century lead to great suffering, inequality, lack of social protections, and whips of the business cycle
* Government acts in correcting for market inefficiencies:
    * monopolies (e.g. monopolies, over-regulation) 
    * externalities(e.g. pollution) 
    * ensure public goods (e.g. lighthouses)
* Since the macroeconomic development it also used fiscal policies(taxation & spending) and monetary policies (credit & interest rates) to promote economic growth and stability
* It also ensures economic inequality doesn't go rampant via redistribution policies

# Chapter 3: Basic elements of supply and demand

## Demand schedule

The market demand curve is found by adding together the quantities demanded by individuals at all price levels.

Law of downward-sloping demand: as price increases, the demand at that price level decreases. It's mostly due to two reasons:
* substitution effect --> consumers switch to related good as they become cheaper than the original good in question
* income effect --> consumer has less money available for purchasing goods, thus they buy less of it.

* market size, that is the number of actors. It's different in 40MM country vs 5MM country
* average income --> increase in the average income might increase the demand
* consumer taste & preferences
* prize and availability of related good --> how does typewriter demand changes as PC prices fell
* special influences 
    * e.g. some legal requirements to have something, like first aid kit in the cars
    * umbrellas in rainy cities vs in arid climate

## Supply schedule

The supply curve is a relationship between its market price and the maximum producable amout at that market price

It's influences by the following things:
* cost of production
	* technological advances
	* inputs price
* price of related goods --> e.g. if it's more profitable to produce corn vs. soybeans the production shall switch
* government policies --> quotas, rules & regulations
* special influences --> Internet shopping and auctions allow consumers to compare the prices of different dealers more easily and drives high-cost sellers out of business.

## Equilibrium of supply and demand

