---
title: "Pragmatic execution tracing"
date: 2018-10-05 
abstract: |
  This talk covers contemporary execution tracing technologies; from gathering execution traces, storage, and analysis.

  Optimizing application latency and speed is a difficult challenge since many factors could lead to slowdown --> network IO, CPU scheduler, waiting on a mutex, database, other services... Many of those non-CPU intensive activities don't show on the traditional profilers (pprof) and aren't visible on flame graphs. Tracing method execution calls in time, from the moment it starts, until it ends with adding metadata enables us a deeper insight into our program, hidden otherwise. Additionally distributed tracing tracks request over multiple services, complementing logging for the microservice architecture.

  This talk covers the most simple Chrome Trace event format, and briefly present two frameworks for distributed tracing opentracing.io and OpenCensus.
blogpost: 2018-pragmatic-tracing
appearances:
- name: Webcamp Zagreb 
  url: https://2018.webcampzg.org/talks/pragmatic-execution-tracing/
  yt: kN9PjsDhH-I
  date: 2018-10-05
  slides: https://slides.com/nmiculinic/pragmatic-tracing-5/fullscreen#/
---