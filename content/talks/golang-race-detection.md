---
title: "Golang race detection"
date: 2019-01-29
abstract: |
    Data races are nasty kinds of bugs; rare, hard to replicate and tend to occur at the worst possible moment. Their effect is undefined, detection hard, almost impossible without expensive formal verification and static analysis tools...Or is it? 

    This talk focuses on [ThreadSanitizer](https://clang.llvm.org/docs/ThreadSanitizer.html), a library for detecting race conditions at run-time. It originated in clang & C++ community and its use spread to go (-race), rust, java, and some other languages. 

    It covers how it works conceptually, and necessary background for its understanding.
blogpost: 2019-golang-race-detection
appearances:
- name: Gophercon Russia
  url: https://www.gophercon-russia.ru/en
  date: 2019-04-13
  yt: 4wq-YlaI_vk
  slides: https://docs.google.com/presentation/d/19mLkWBlniCXHxG-DobTSpX_Z-xJZOxMOYMsmxqNjcGs/edit?usp=sharing
- name: GoDays Berlin
  url: https://www.godays.io/speakers
  date: 2019-01-29
  slides: https://docs.google.com/presentation/d/19mLkWBlniCXHxG-DobTSpX_Z-xJZOxMOYMsmxqNjcGs/edit?usp=sharing
---