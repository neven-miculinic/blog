---
draft: true
title: "What does the kubelet say?"
date: 2019-06-25
abstract: |
    Etcd says store, kube-proxy says route, API server says 418, what does the kubelet say?

    Kubelet is one of the central components in the kubernetes cluster. Most of us are taking it for granted that is would just work and start our containers. CNI handles the networking part, kube-proxy the service part, but kubelet does more than just starting containers. In this talk, I cover kubelet on a high level before deep diving in the belly of the beast and its interfacing with CNI, container runtime and ultimately Linux kernel.
tags:
  - "kubernetes"
---

# References
* [SIG Node Deep Dive – Dawn Chen, Google (Intermediate Skill Level)](https://www.youtube.com/watch?v=I4AchBfe49U)
* [Kubernetes Node Deep Dive 2017](https://www.slideshare.net/resouer/k8s-meetup-3june-77322267)