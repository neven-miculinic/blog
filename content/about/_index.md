---
title: "About"
description: ""
featured_image: ''
---

## One sentence 
I'm a system software developer with some devops skills in the middle. Likes distributed systems, kubernetes and everything in between


## Short (~150 words)
Neven’s parents committed a grave mistake, and at the age of 12 introduced him to Pascal and Logo. Since then he's programmed in various languages, from C, C++ to Haskell and Go, took part in algorithm competitions, and finished his Master degree in Computer Science. He saw his first big system during his Facebook 2015 summer internship, and since then worked on various jobs from building a DNA deep learning base-caller to a real-time cryptocurrency bot in Go.  

Currently, his passion lies in DevOps, Kubernetes and the CNCF landscape, as well as performance engineering, monitoring, and system observability.

Neven also created wireguard CNI ansible roles, wg-quick-go wireguard library and wg-operator designed to manage VPN private/public keys via the kubernetes API server.

## CV

Full CV can be found at [this](/nmiculinic_cv.pdf) link.