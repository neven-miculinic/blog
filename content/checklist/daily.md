# Morning routine

* [ ] Wake up
* [ ] drink 40mg Spasmomen
* [ ] brush teeth
* [ ] eye drops
* [ ] morning skin care routine`
* [ ] morning meditation
* [ ] eat breakfast
* [ ] take medications
    * [ ] cymbalta 60mg
    * [ ] Aroxia 60mg
    * [ ] Salzapoyrin 60mg
    * [ ] Omega 3 
* [ ] Check todoist

* [ ] eat brunch 
* [ ] speech exercices
* [ ] eye drops 
* [ ] check email
* [ ] check social sites
    * [ ] what's app
    * [ ] messenger
    * [ ] riot
    * [ ] tinder
    * [ ] firefox .P profile
    * [ ] firefox .P email
* [ ] eat lunch
* [ ] Omega 3
* [ ] eye drops
* [ ] eat a meal
* [ ] eye drops
* [ ] eat dinner
* [ ] Omega 3
* [ ] eye drops
* [ ] wash the dishes
* [ ] brush the teeth
* [ ] put the dental retainers
* [ ] evening skin care routine
* [ ] read something for a 30 min
* [ ] plan the next day
* [ ] print next days checklist